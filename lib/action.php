<?php
class TableAction extends DbrBaseUtils {
	public function __construct() {
		parent::__construct($_GET);
		if (isset($_GET['remove'])) $this->removeRow($_GET['remove']);
		if (isset($_GET['edit'])) $this->editField($_GET['edit']);
		return true;
	}
	public function removeRow($remove) {
		if (!is_array($remove)) return false;
		$query = 'delete from '.$remove['database'].'.'.$remove['table'].'
		where '.$remove['table'].'.'.$remove['key'].' = "'.$remove['id'].'"';
		return $this->db->query($query);
	}
	public function editField($edit) {
		if (!is_array($edit)) return false;

		if (strtolower($edit['value']) == 'null') {
			$edit['value'] = 'NULL';
		} else {
			$edit['value'] = '\''.$edit['value'].'\'';
		}
		$query = 'update '.$edit['database'].'.'.$edit['table'] .'
		set '.$edit['table'].'.'.$edit['column'].' = '.$edit['value'].'
		where '.$edit['table'].'.'.$edit['key'].' = \''.$edit['id'].'\'
		limit 1';
		$this->db->query($query);
		header('Location: '.$edit['backUrl']);
		exit;
	}
}