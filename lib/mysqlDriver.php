<?php
class DbrMySqlFactory {
	private $connection;
	private $user = 'root';
	private $password = '';
	public $host = 'localhost';
	public $database = 'commerce';
	public $tbx = true;
	public function __construct($database = '') {
		$this->connect($database);
	}
	public function connect($database = '') {
		if ($this->tbx) {
			require_once '/opt/applications/textbookx/php-lib/bootstrap.php';
			require_once '/opt/applications/textbookx/php-lib/lib_db.php';
			require_once '/opt/applications/textbookx/php-lib/lib_sql.php';
			$this->connection = dbConnect();
            if(!empty($database)) {
                $this->database = $database;
            }
			return $this->connection;
		}
		$this->connection = mysql_connect($this->host, $this->user, $this->password);
		if (!$this->connection) {
			$message = 'Error connecting to host <strong>'.$this->host.'</strong> user <strong>'.$this->user.'</strong>: ';
			$message .= mysqlError();
			return DbrBaseUtils::err($message);
		}
		if (empty($database)) return $this->connection;
		if (!$this->selectDb($database))  {
			DbrBaseUtils::err('Can not select database. '.mysqlError());
			return false;
		}

		return $this->connection;
	}
	public function selectDb($database) {
		if (!mysqlSelectDB($database)) return false;
		$this->database = $database;
		return true;
	}
	public function query($query) {
		if (empty($query)) return false;
		if (empty($this->connection)) $this->connect();
		$res = mysqlQuery($query, $this->connection);
		if($res == false) {
			DbrBaseUtils::err(mysqlError($this->connection).'<br><br> Query:<br>'.$query);
			return false;
		}
		return $res;
	}
	public function fetch($query) {
		$res = $this->query($query);
		if (mysqlNumRows($res) == 0) return array();
		$rows = array();
		while ($row = mysqlFetchAssoc($res)) {
			if (count($row) == 1) $row = current($row);
			$rows[] = $row;
		}
		mysqlFreeResult($res);
		return $rows;
	}
	public function row($query) {
		return current($this->fetch($query));
	}
	public function value($query) {
		$result = $this->fetch($query);
		return is_array($result) ? current($result) : $result;
	}
	public function close() {
		if (empty($this->connection)) return false;
		return mysqlClose($this->connection);
	}
	public function checkCredentials($user, $password) {
		return $user === $this->user and $password === $this->password;
	}
}
