var form;

function setTableList(response) {
	var form = document.forms['dbSelect'];
	addOptions(form.table, response);
}
function addOptions(list, items) {
	items = items.split(',');
	list.length = 0;
	//list.disabled = false;
	for (var i = 0; i < items.length; i++) {
		createOption(list, items[i], items[i], i);
	}
}
function createOption(list, text, value, index) {
	if (!document.createElement) {
		list.options[index] = new Option(text, value, false, false);
		return true;
	}
	var newListOption = document.createElement('option');
	newListOption.text = text;
	newListOption.value = value;
	if (list.options.add) return list.options.add(newListOption)
	return list.add(newListOption, null);
}
function hideOptions(list) {
	list.length = 0;
	createOption(list, 'Loading tables.', '0', 0);
	//list.disabled = true;
}
window.onload = function() {
	var form = document.forms['dbSelect'];
	if (!form) return false;
	var database = form.elements['database'];

	database.onchange = function(e) {
		var obj = window.event ? window.event.srcElement : e.target;
		hideOptions(form.table);
		var url = 'returner.php?database=' + obj.value;
		getRequest(url, function(req) { setTableList(req.responseText); } );
	};
}