<?php
require 'lib/core.php';
$dbr = new DbrBaseUtils($_GET);
$query = 'select * from '.$dbr->database.'.'.$_GET['table'].' where 1 = 1 ';
if (!is_array($_GET['search'])) $_GET['search'] = array();
foreach ($_GET['search'] as $field => $value) {
	if (empty($value)) continue;
	$query .= 'and '.$dbr->database.'.'.$_GET['table'].'.'.$field.' = \''.$value.'\' ';
}
$query .= 'limit 20';
$row = $dbr->db->fetch($query);
?>
<table class="data">
<?php if (!is_array($row) || empty($row)) : ?>
<tr><th>
	status
</th><td>
	No information
</td></tr>
<?php else :
$row = current($row);
foreach ($row as $key => $value) {
	if (strlen($value) > 64) $row[$key] = strip_tags(substr($value, 0, 64)).'...';
}
endif ?>
<?php foreach ($row as $field => $value) :
		if (empty($value)) $value = '&mdash;';
?>
	<tr><th align="right"><?=$field?>:</th><td><?=$value?></td></tr>
<?php endforeach ?>
</table>